<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd" [
<!ENTITY appname "GNOME Mastermind">
<!ENTITY app "<application>GNOME Mastermind</application>">
<!ENTITY appversion "0.3">
<!ENTITY manrevision "0.2">
<!ENTITY date "May 2007">
<!-- Information about the entities
       Use the app and appname entities to specify the name of the application.  
       Use the appversion entity to specify the version of the application.
       Use the manrevision entity to specify the revision number of this manual.
       Use the date entity to specify the release date of this manual.
-->]>
<!-- 
  This is a GNOME documentation template, designed by the GNOME
  Documentation Project Team. Please use it for writing GNOME
  documentation, making obvious changes, such as:
  - all the words written in UPPERCASE (with the exception of GNOME) 
    should be replaced. As for "legalnotice", please leave the reference
    unchanged.
  - all id attributes should have 'myapp' replaced

  Remember that this is a guide, rather than a perfect model to follow
  slavishly. Make your manual logical and readable: see the GDP Style Guide for
 more on this.
-->
<!--
      (Do not remove this comment block.)
  Maintained by the GNOME Documentation Project
  http://live.gnome.org/DocumentationProject
  Template version: 3.0 beta
  Template last modified 2006-11-21

-->
<!-- =============Document Header ============================= -->
<article id="index" lang="fr">
  <articleinfo> 
    <title>Manuel de <application>GNOME Mastermind</application></title>
    <abstract role="description">
      <para><application>GNOME Mastermind</application> est un jeu similaire à <trademark class="trade">Mastermind</trademark> pour le bureau GNOME.</para>
    </abstract>         

    <copyright> 
      <year>2007</year> 
      <holder>Filippo Argiolas</holder> 
    </copyright><copyright><year>2007.</year><holder>Robert-André Mauchin (zebob.m@gmail.com)</holder></copyright> 

    <!-- An address can be added to the publisher information. -->
    <publisher role="maintainer"> 
      <publishername>Filippo Argiolas</publishername> 
    </publisher> 

	  <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <!-- The file legal.xml contains link to license for the documentation, 
        and other legal stuff such as "NO WARRANTY" statement. 
        Please do not change any of this. -->

    <authorgroup> 
      <author role="maintainer"> 
    		<firstname>Filippo</firstname> 
    		<surname>Argiolas</surname> 
		<affiliation>
		  <address>
		    <email>filippo.argiolas@gmail.com&gt;</email>
		  </address>
		</affiliation>
      </author> 
      <corpauthor>Filippo Argiolas</corpauthor> 
    </authorgroup>
    

<!-- According to GNU FDL, revision history is mandatory if you are -->
<!-- modifying/reusing someone else's document.  If not, you can omit it. -->
<!-- Remember to remove the &manrevision; entity from the revision entries other
-->
<!-- than the current revision. -->
<!-- The revision numbering system for GNOME manuals is as follows: -->
<!-- * the revision number consists of two components -->
<!-- * the first component of the revision number reflects the release version of the GNOME desktop. -->
<!-- * the second component of the revision number is a decimal unit that is incremented with each revision of the manual. -->
<!-- For example, if the GNOME desktop release is V2.x, the first version of the manual that -->
<!-- is written in that desktop timeframe is V2.0, the second version of the manual is V2.1, etc. -->
<!-- When the desktop release version changes to V3.x, the revision number of the manual changes -->
<!-- to V3.0, and so on. -->  
    <revhistory>
      <revision> 
		<revnumber>Manuel de GNOME Mastermind V0.3</revnumber> 
		<date>May 2007</date> 
		<revdescription> 
	  		<para role="author">Filippo Argiolas</para>
	  		<para role="publisher">Filippo Argiolas</para>
		</revdescription> 
      </revision> 
    </revhistory> 

    <releaseinfo>Ce manuel présente la version 0.3 de GNOME Mastermind.</releaseinfo> 
    <legalnotice> 
      <title>Commentaires et corrections</title> 
    <para>Veuillez rapporter n'importe quelle anomalie au <ulink url="https://gna.org/bugs/?func=additem&amp;group=gnome-mastermind">traqueur d'anomalies de gna.org</ulink> ou inscrivez-vous sur la liste de diffusion publique sur la page web du projet pour nous faire part de vos commentaires et corrections.</para>
    <!-- Translators may also add here feedback address for translations -->
    </legalnotice> 
  </articleinfo> 

  <indexterm zone="index"> 
    <primary><application>GNOME Mastermind</application></primary> 
  </indexterm> 

<!-- ============= Document Body ============================= -->
<!-- ============= Introduction ============================== -->
<!-- Use the Introduction section to give a brief overview of what
     the application is and what it does. -->
  <sect1 id="myapp-introduction"> 
    <title>Introduction</title> 
    <sect2 id="the-game">
      <title>Le jeu</title>
      <para>Voici pour débuter une brève introduction à propos du jeu classique auquel celui-ci ressemble (source : wikipedia).</para>
      <para><trademark class="trade">Mastermind</trademark> est un jeu de cassage de code pour deux joueurs, inventé en 1970 par Mordecai Meirowitz, qui a été très populaire durant les années 80.</para>
      <para>Le jeu se joue avec deux joueurs : le <emphasis>créateur de code</emphasis> et le <emphasis>casseur de code</emphasis>. Le créateur de code, place de son côté du plateau (caché de l'autre), une combinaison de 4 couleurs. Le casseur de code doit deviner ce motif (à la fois l'ordre et les couleurs) en un nombre minimal de coups. À chaque tour (12 dans la version la plus typique), le casseur de code tente un motif et le créateur de code lui donne un retour dans la grille de score en plaçant un pion blanc pour chaque élément de bonne couleur mais mal placé et un pion coloré pour les éléments à la fois de bonne couleur et bien placé.</para>
      <para>Une fois qu'il a eu le retour, le casseur de code essaye à nouveau de deviner la séquence en suivant les astuces du créateur de code. Le jeu est évidemment gagné par le créateur de code si le casseur de code n'a réussi aucune tentative.</para>
    </sect2>
    <sect2 id="this-game">
      <title>Ce jeu</title>
      <para><application>GNOME Mastermind</application> est une version solitaire qui ressemble à la version classique. Le rôle du créateur de code est pris par l'ordinateur.</para>
      <para>Le joueur doit deviner une combinaison de quatre couleurs (notez que chaque couleur peut être répétée). L'ordinateur affiche (avec le thème par défaut sélectionné) un pion rouge pour chaque bonne couleur correctement placée et un pion blanc pour chaque bonne couleur mal placée.</para>
    </sect2>
    <sect2 id="get-started">
      <title>Bien débuter</title>
      <para>Vous pouvez lancer <application>GNOME Mastermind</application> des manières suivantes :</para> 
      <variablelist>
    	<varlistentry>
    	  <term>Depuis le menu <guimenu>Applications</guimenu>,</term>
    	  <listitem>
    	    <para>Choisissez <menuchoice><guisubmenu>Jeux</guisubmenu><guimenuitem>Mastermind</guimenuitem></menuchoice>.</para>
    	  </listitem>
    	</varlistentry>
    	<varlistentry>
    	  <term>Ligne de commande</term>
    	  <listitem>
    	    <para>Pour démarrer <application>GNOME Mastermind</application> depuis la ligne de commande, saisissez la commande suivante, puis appuyez sur <keycap>Entrée</keycap>:</para>
    	    <para> 
	      <command>gnome-mastermind</command> 
    		</para> 
    	  </listitem>
    	</varlistentry>
      </variablelist>

    <para>Voici une capture d'écran de <application>GNOME Mastermind</application>, au cours d'une partie classique</para>

    <!-- ==== Figure ==== -->
      <figure id="mainwindow-fig"> 
	<title><application>GNOME Mastermind</application> Fenêtre de démarrage</title> 
	<screenshot> 
	  <mediaobject> 
	    <imageobject><imagedata fileref="figures/mainwindow.png" format="PNG"/> 
	    </imageobject>
	    <!-- EPS versions of the figures are not required at the moment. -->
            <!-- 
		    <imageobject>
      		<imagedata fileref="figures/image.eps" format="EPS"/>
    		    </imageobject>
	    -->
	    <textobject> 
	      <phrase>Shows <application>GNOME Mastermind</application> main window. Contains titlebar, menubar, toolbar, display area, and
	      scrollbars. Menubar contains File, View, Settings, and Help menus. </phrase> 
	    </textobject>
	  </mediaobject> 
	</screenshot> 
      </figure>
    <!-- ==== End of Figure ==== -->

    <!-- Include any descriptions of the GUI immediately after the screenshot of the main UI, -->
    <!-- for example, the items on the menubar and on the toolbar. This section is optional. -->

    <para>Jetons tout d'abord un œil à l'interface de jeu. De haut en bas :</para>
      <variablelist>
	<varlistentry>
	<term>Barre de menu.</term>
	<listitem>
	<para>Le menu contient toutes les commandes dont vous avez besoin pour utiliser <application>GNOME Mastermind</application>.</para>
	</listitem>
	</varlistentry>
	<varlistentry>
	<term>Barre d'outils.</term>
	<listitem>
	<para>La barre d'outils contient un jeu de commandes auxquelles vous accédez généralement le plus souvent.</para>
	</listitem>
	</varlistentry>
	<varlistentry>
	<term>Zone de jeu principale.</term>
	<listitem>
	<para>De gauche à droite vous pouvez voir la grille principale où les pions sont placés, et la grille de score où l'ordinateur renvoie les résultats</para>
	</listitem>
	</varlistentry>
	<varlistentry>
	<term>zone des pions.</term>
	<listitem>
	<para>C'est la zone des pions colorés. Cliquez ici avec le bouton gauche de la souris pour sélectionner un pion. Cliquez ensuite dans une case libre de la grille principale pour placer le pion.</para>
	<para>Un clic du milieu sur un pion dans la zone des pion place ce dernier dans la première case disponible.</para>
	</listitem>
	</varlistentry>
	</variablelist>
    </sect2>
    <sect2 id="scuts">
      <title>Raccourcis</title>
      <itemizedlist> 
	<listitem> 
	  <para> 
	    <guilabel>Clic gauche</guilabel> </para> 
	  <para>Sélectionne un pion si effectué dans la zone des pions. Le positionne si effectué dans les cases de la grille principale.</para>
	</listitem> 
	<listitem>
	  <para>
	    <guilabel>Glisser-déposer avec le bouton gauche</guilabel> </para>
	  <para>Positionne le pion en le déplaçant de la zone des pions vers une case active.</para>
	</listitem>
	<listitem> 
	  <para><guilabel>Double clic avec le bouton de gauche</guilabel> ou <guilabel>clic avec le bouton du milieu</guilabel></para> 
	  <para>Disponible dans la zone des pions, place un pion dans la première case libre.</para>
	</listitem> 
	<listitem> 
	  <para> 
	    <guilabel>Ctrl + Z</guilabel> </para> 
	  <para>Annule le dernier coup.</para> 
	</listitem> 
	<listitem> 
	  <para> 
	    <guilabel>Shift + Ctrl + Z</guilabel> </para> 
	  <para>Rétablit le dernier coup annulé.</para> 
	</listitem> 
	<listitem> 
	  <para> 
	    <guilabel>Ctrl + N</guilabel> </para> 
	  <para>Démarre une nouvelle partie.</para> 
	</listitem> 
	<listitem> 
	  <para> 
	    <guilabel>Ctrl + Q</guilabel> </para> 
	  <para>Quitte la fenêtre de jeu.</para> 
	</listitem> 
	<listitem> 
	  <para> 
	    <guilabel>Ctrl + P</guilabel> </para> 
	  <para>Oouvre la boîte de dialogue des préférences.</para> 
	</listitem> 
	<listitem> 
	  <para> 
	    <guilabel>F1</guilabel> </para> 
	  <para>Affiche ce manuel.</para> 
	</listitem> 
      </itemizedlist> 
      <note>
	  <itemizedlist>
	    <listitem>
	      <para>Vous pouvez également placer des pions <emphasis>par dessus</emphasis> les pions déjà positionnés (sur la ligne en cours).</para>
	    </listitem>
	    <listitem>
	      <para>Positionner le dernier pion sur une ligne démarre la vérification de cette dernière (sans possibilité d'annuler le dernier coup ou de remplacer un pion). Soyez prudent si vous n'êtes pas sûr(e).</para>
	    </listitem>
	  </itemizedlist>
      </note>
    </sect2>
    <sect2>
    <title id="walkthrough">Exemple d'une partie</title>
    <para>Regardons maintenant la précédente partie pas à pas :</para>
    <!-- ==== Figure ==== -->
      <figure id="row1-fig"> 
	<title><application>GNOME Mastermind</application> Première rangée</title> 
	<screenshot> 
	  <mediaobject> 
	    <imageobject><imagedata fileref="figures/row1.png" format="PNG"/> 
	    </imageobject>
	    <!-- EPS versions of the figures are not required at the moment. -->
            <!-- 
		    <imageobject>
      		<imagedata fileref="figures/image.eps" format="EPS"/>
    		    </imageobject>
	    -->
	    <textobject> 
	      <phrase>Première rangée</phrase>
	    </textobject>
	  </mediaobject> 
	</screenshot> 
      </figure>
    <!-- ==== End of Figure ==== -->
    <para>C'est le premier coup d'essai. Comme vous pouvez le voir la grille de score est vide car aucun bon pion n'a été trouvé.</para>
    <!-- ==== Figure ==== -->
      <figure id="row2-fig"> 
	<title><application>GNOME Mastermind</application> Deuxième rangée</title> 
	<screenshot> 
	  <mediaobject> 
	    <imageobject><imagedata fileref="figures/row2.png" format="PNG"/> 
	    </imageobject>
	    <!-- EPS versions of the figures are not required at the moment. -->
            <!-- 
		    <imageobject>
      		<imagedata fileref="figures/image.eps" format="EPS"/>
    		    </imageobject>
	    -->
	    <textobject> 
	      <phrase>Deuxième rangée</phrase>
	    </textobject>
	  </mediaobject> 
	</screenshot> 
      </figure>
    <!-- ==== End of Figure ==== -->
    <para>Nous avons ici trois pions de score blancs. Ceci signifie que nous avons trouvé trois bonnes couleurs mais qu'elles sont placées dans le mauvais ordre. Nous suivons le bon chemin.</para>
    <!-- ==== Figure ==== -->
      <figure id="row3-fig"> 
	<title><application>GNOME Mastermind</application> Troisième rangée</title> 
	<screenshot> 
	  <mediaobject> 
	    <imageobject><imagedata fileref="figures/row3.png" format="PNG"/> 
	    </imageobject>
	    <!-- EPS versions of the figures are not required at the moment. -->
            <!-- 
		    <imageobject>
      		<imagedata fileref="figures/image.eps" format="EPS"/>
    		    </imageobject>
	    -->
	    <textobject> 
	      <phrase>Troisième rangée</phrase>
	    </textobject>
	  </mediaobject> 
	</screenshot> 
      </figure>
    <!-- ==== End of Figure ==== -->
    <para>Laisser la même combinaison de couleurs mais changer l'ordre nous mène à la solution. Mais nous devons toujours chercher lequel est mauvais !</para>
    <!-- ==== Figure ==== -->
      <figure id="row4-fig"> 
	<title><application>GNOME Mastermind</application> Quatrième rangée</title> 
	<screenshot> 
	  <mediaobject> 
	    <imageobject><imagedata fileref="figures/row4.png" format="PNG"/> 
	    </imageobject>
	    <!-- EPS versions of the figures are not required at the moment. -->
            <!-- 
		    <imageobject>
      		<imagedata fileref="figures/image.eps" format="EPS"/>
    		    </imageobject>
	    -->
	    <textobject> 
	      <phrase>Quatrième rangée</phrase>
	    </textobject>
	  </mediaobject> 
	</screenshot> 
      </figure>
    <!-- ==== End of Figure ==== -->
    <para>Insérer une couleur aléatoire affiche un pion de score blanc. Comme nous n'avons pas changé les autres pions colorés, nous avons donc trouvé la couleur manquante !</para>
    <!-- ==== Figure ==== -->
      <figure id="row5-fig"> 
	<title><application>GNOME Mastermind</application> Dernière rangée</title> 
	<screenshot> 
	  <mediaobject> 
	    <imageobject><imagedata fileref="figures/row5.png" format="PNG"/> 
	    </imageobject>
	    <!-- EPS versions of the figures are not required at the moment. -->
            <!-- 
		    <imageobject>
      		<imagedata fileref="figures/image.eps" format="EPS"/>
    		    </imageobject>
	    -->
	    <textobject> 
	      <phrase>Dernière rangée</phrase>
	    </textobject>
	  </mediaobject> 
	</screenshot> 
      </figure>
    <!-- ==== End of Figure ==== -->
    <para>Oui ! Remettre le pion enlevé et remplacer un autre par la nouvelle bonne couleur résout tout. Nous avons gagné :D !</para>
    </sect2>
    <sect2 id="cmsoon">
      <title>Et bientôt</title> 
      <para>Les prochaines versions contiendront un manuel plus détaillé qui expliquera mieux chaque réglages du menu préférences. Il présentera aussi un guide simple pour dessiner et envoyer de nouveaux thèmes.</para>
    </sect2>
  </sect1>

<!-- ============= About ================================== -->
<!-- This section contains info about the program (not docs), such as
      author's name(s), web page, license, feedback address. This
      section is optional: primary place for this info is "About.." box of
      the program. However, if you do wish to include this info in the
      manual, this is the place to put it. Alternatively, you can put this information in the title page.-->
  <sect1 id="myapp-about"> 
    <title>À propos <application>GNOME Mastermind</application></title> 
    <para><application>GNOME Mastermind</application> a été écrit par Filippo Argiolas (<email>filippo.argiolas@gmail.com</email>). Pour davantage d'information à propos <application>GNOME Mastermind</application>, visitez la <ulink url="http://www.autistici.org/gnome-mastermind" type="http">page web <application>GNOME Mastermind</application></ulink>(en anglais).</para>
    <para>Pour les problèmes de droits d'utilisation vérifiez la boîte de dialogue À propos et également les fichiers copyright et copyright-artwork dans les sources du programme ou dans le répertoire des données du jeu (typiquement /usr/share/gnome-mastermind).</para>
    <para>Veuillez rapporter n'importe quelle anomalie au <ulink url="https://gna.org/bugs/?func=additem&amp;group=gnome-mastermind">traqueur d'anomalies de gna.org</ulink> ou inscrivez-vous sur la liste de diffusion publique sur la page web du projet pour nous faire part de vos commentaires et corrections.</para>
    <para>Ce programme est distribué suivant les termes de la Licence Publique Générale GNU telle que publiée par la Free Software Foundation ; soit la version 2 de cette licence, soit (à votre convenance) une version ultérieure. Une <ulink url="ghelp:gpl" type="help">copie de cette licence</ulink> est incluse avec cette documentation ; une autre peut être trouvée dans le fichier COPYING inclus avec le code source de ce programme.</para>

  </sect1> 
</article>
