gnome-mastermind (0.3.1-5) unstable; urgency=medium

  * QA upload.

  [ Debian Janitor ]
  * Use secure URI in Homepage field.
  * Set debhelper-compat version in Build-Depends.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 04 Feb 2021 01:31:15 +0000

gnome-mastermind (0.3.1-4) unstable; urgency=medium

  * QA upload.

  [ Jeremy Bicha ]
  * Add minimal debian/gbp.conf

  [ Yavor Doganov ]
  * debian/patches/gsettings-port.patch: Remove GConf migration code.
  * debian/patches/gtk3-port.patch: Refresh.
  * debian/compat: Set to 12.
  * debian/control (Recommends): Remove gconf2.
    (Build-Depends): Bump debhelper requirement.
    (Rules-Requires-Root): Set to no.
    (Standards-Version): Bump to 4.4.1; no changes needed.
  * debian/copyright: Update copyright years.

 -- Yavor Doganov <yavor@gnu.org>  Sun, 20 Oct 2019 14:04:27 +0300

gnome-mastermind (0.3.1-3) unstable; urgency=medium

  * QA upload.
  * debian/source/format: New file; set format to 3.0 (quilt).
  * debian/patches/01_docs.diff: Add description.  Patch Makefile.am
    instead of Makefile.in.  Add TODO to doc_DATA.
  * debian/patches/use-yelp-tools.patch: New; move away from the
    deprecated gnome-doc-utils (Closes: #829977).
  * debian/patches/gsettings-port.patch: New; switch from GConf to
    GSettings (Closes: #886075).
  * debian/patches/no-overlinking.patch: New; avoid linking the needed
    libraries twice.
  * debian/patches/gtk3-port.patch: New; port to GTK+ 3.
  * debian/patches/desktop-file.patch: New; make .desktop file valid.
  * debian/patches/series: New file.
  * debian/compat: Set to 11.
  * debian/control: Run wrap-and-sort -ast.
    (Maintainer): Set to the Debian QA Group (O: #826926).
    (Build-Depends): Bump debhelper requirement to >= 11.  Remove cdbs,
    autotools-dev, docbook-xml, imagemagick, gnome-doc-utils,
    libxml-parser-perl and libgconf2-dev.  Add intltool and yelp-tools.
    Replace libgtk2.0-dev with libgtk-3-dev.  Remove obsolete version
    requirements for libglib2.0-dev and pkg-config.
    (Recommends): Add gconf2 for data migration's sake.
    (Description): Extend.
    (Vcs-Git, Vcs-Browser): Add.
    (Standards-Version): Claim compliance with 4.2.1 as of this release.
  * debian/rules: Rewrite for plain dh; enable hardening.
  * debian/install: Delete.
  * debian/menu: Delete as required by current Policy.
  * debian/gnome-mastermind.6: A brand new one.
  * debian/manpages: New file; install it.
  * debian/watch: Replace with a fake one as Gna! is gone.
  * debian/changelog: Whitespace cleanup.
  * debian/copyright: Rewrite in copyright-format 1.0.

 -- Yavor Doganov <yavor@gnu.org>  Sat, 03 Nov 2018 01:49:41 +0200

gnome-mastermind (0.3.1-2) unstable; urgency=low

  * debian/control: Build-Depends: docbook-xml.  Closes: #676751.

 -- Bart Martens <bartm@debian.org>  Sun, 10 Jun 2012 18:15:21 +0000

gnome-mastermind (0.3.1-1) unstable; urgency=low

  * New upstream release.
  * debian/copyright: Updated.

 -- Bart Martens <bartm@debian.org>  Sat, 26 Apr 2008 17:19:23 +0200

gnome-mastermind (0.3-1) unstable; urgency=low

  * New upstream release.
  * debian/patches/02_dfsg.1.diff, debian/README.Debian-source: Removed.
  * debian/copyright: Updated.

 -- Bart Martens <bartm@knars.be>  Thu, 31 May 2007 06:55:19 +0200

gnome-mastermind (0.2+dfsg.1-1) unstable; urgency=low

  * Initial package in Debian.  Closes: #422424.  This package was requested
    by Filippo Argiolas <filippo.argiolas@gmail.com>.
  * debian/README.Debian-source: Documents how the .orig.tar.gz was made.
  * debian/patches/02_dfsg.1.diff: Added.  This patch contains the changes
    that are needed because of the repackaged .orig.tar.gz file.

 -- Bart Martens <bartm@knars.be>  Sat, 12 May 2007 08:59:49 +0200
